extends RigidBody2D

#signals
signal destroyed

#objects to instance
var sparksObj = load("objects/sparks/sparks.tscn")
var explodeObj = load("objects/explode/explode.tscn")
var planetPieces = [ 
	"objects/planet_parts/part0/PlanetPart0.tscn",
	"objects/planet_parts/part1/PlanetPart1.tscn",
	"objects/planet_parts/part2/PlanetPart2.tscn",
	"objects/planet_parts/part3/PlanetPart3.tscn",
	"objects/planet_parts/part4/PlanetPart4.tscn",
	"objects/planet_parts/part5/PlanetPart5.tscn"
]

#components
var sound

var hp = randi()%6+1
var pieces = hp
var owned = false
var isDead = false
var color1
var color2


func _ready():
	get_node("/root/Main").planets += 1
	
	sound = get_node("sound")

func make_owned():
	owned = true

func report_colors(c1,c2):
	color1 = c1
	color2 = c2

func start_explode():
	isDead = true
	get_node("/root/Main").planets -= 1
	remove_from_group("planet")
	
	sound.play("explode")
	sound.set_pitch_scale(0, randf(0.8,1.5))
	emit_signal("destroyed")
	
	#spawn planet pieces
	for i in range(0,pieces):
		var piece = load(planetPieces[randi()%planetPieces.size()]).instance()
		piece.set_pos( get_pos() + Vector2( rand_range(-7,7), rand_range(-7,7) ) )
		piece.set_color(color1, color2)
		get_parent().add_child(piece)
	
	#make explosion effect
	var explode = explodeObj.instance()
	explode.set_pos(get_pos())
	explode.set_emitting(true)
	get_parent().add_child(explode)
	
	#later do explosion TODO
	get_node("PlanetFace").hide()
	
	#start remove timer to let sound play
	get_node("removeTimer").start()
	
	

func _on_planet_body_enter( body ):
	if body.is_in_group("planet") :
		#get planet speed
		var speed = body.get_linear_velocity().length()
		if owned:
			speed = get_linear_velocity().length()
		else:
			#make sparks
			var contact = get_pos() + (body.get_pos() - get_pos()).normalized()
			var sparks = sparksObj.instance()
			sparks.set_pos(contact)
			sparks.set_emitting(true)
			sparks.set_scale( Vector2( 1*(speed/300), 1*(speed/300) ) )
			get_parent().add_child(sparks)
		
		#set sound volumn
		sound.set_volume(0, 0.5*(speed/300))
		
		#only subtract if the colliding body isn't the player
		if not body.is_in_group("player"):
			#subtract health
			if speed > 200:
				hp -= 1
				if hp <= 0:
					start_explode()
				else:
					
					sound.play("hit")
		else:
			sound.play("hit")

func _on_player_hurt_me():
	if not isDead:
		#make sparks
		var sparks = sparksObj.instance()
		sparks.set_pos(get_pos())
		sparks.set_emitting(true)
		get_parent().add_child(sparks)
		
		hp -= 1
		if hp <= 0:
			start_explode()
		else:
			sound.play("hit")


func _on_removeTimer_timeout():
	call_deferred("queue_free")
