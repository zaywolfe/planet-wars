extends RigidBody2D

#signals
signal shoot
signal hurt_planet

#exports
export var movePower = 1
export(Texture) var chainTexture = null

#loaded objects to spawn
var hookObj = load("objects/hook/hook.tscn")
var playerDieObj = load("objects/PlayerDie/PlayerDie.tscn")

#components
var mouth
var spring
var eyeAnim
var mouthAnim
var sound

#control variables
var ui_up = 0
var ui_down = 0
var ui_left = 0
var ui_right = 0

#internal variables
var forwardVect = Vector2(1,0)
var forwardDir = 0.0
var hook = null
var hookFree = true
var planetHeld = false
var planet = null

var gameOver = false

func _enter_tree():
	get_node("/root/Main").player = self

func _ready():
	set_process(true)
	set_process_input(true)
	
	spring = get_node("spring")
	mouth = get_node("mouth")
	mouthAnim = get_node("mouthAnim")
	eyeAnim = get_node("eyeAnim")
	sound = get_node("SamplePlayer2D")
	
func _process(delta):
	#find foward
	var vel = get_linear_velocity()
	if vel.length() > 0:
		forwardVect = vel.normalized()
		forwardDir = atan2(vel.x, vel.y) - 1.5708# - get_rot()
	
	mouth.set_rot(forwardDir - get_rot())
	
	var move = Vector2(ui_right+ui_left, ui_down+ui_up) * movePower
	add_force(Vector2(0,0), move)
	set_applied_force(get_applied_force() * 0.85)
	
	#random eye blink
	if randi()%1001 < 5: eyeAnim.play("blink")
	
	#tell object to draw
	update()

func _draw():
	if planetHeld:
		draw_set_transform_matrix(get_global_transform().inverse())
		
		var direction = planet.get_pos() - get_pos()
		var distance = direction.length()
		var normal = direction.normalized()
		
		for i in range(0, int(distance/6)+1):
			draw_texture(chainTexture, get_pos() + (normal*6*i) - Vector2(3,3))
	elif not hookFree:
		draw_set_transform_matrix(get_global_transform().inverse())
		
		var direction = hook.get_attach_pos() - get_pos()
		var distance = direction.length()
		var normal = direction.normalized()
		
		for i in range(0, int(distance/6)+1):
			draw_texture(chainTexture, get_pos() + (normal*6*i) - Vector2(3,3))
	

#processing input actions
func _input(event):
	if gameOver: return
	
	if(event.is_action("ui_up") ):
		if ( event.is_pressed()):
			ui_up = -1
		else:
			ui_up = 0
	if(event.is_action("ui_down") ):
		if ( event.is_pressed()):
			ui_down = 1
		else:
			ui_down = 0
	if(event.is_action("ui_left") ):
		if ( event.is_pressed()):
			ui_left = -1
		else:
			ui_left = 0
	if(event.is_action("ui_right") ):
		if ( event.is_pressed()):
			ui_right = 1
		else:
			ui_right = 0
	if( event.is_action_released("ui_accept") ):
		emit_signal("shoot")
	if( event.is_action_released("ui_destroy") ):
		emit_signal("hurt_planet")

func _on_Player_shoot():
	if hookFree:
		mouthAnim.play("shoot")
		hookFree = false
		
		hook = hookObj.instance()
		#hook.set_pos(get_pos())
		hook.set_fire( forwardVect, forwardDir)
		hook.connect("hook_lost", self, "_on_hook_lost")
		hook.connect("caught_planet", self, "_on_planet_caught")
		
		add_child(hook)

func _on_hook_lost():
	#eventually make something for the chain to break
	hookFree = true
	hook = null

func _on_planet_caught( nplanet ):
	#set variables
	hook = null
	planet = nplanet
	planetHeld = true
	#let planet know it's owned
	planet.make_owned()
	#connect signals
	planet.connect("destroyed", self, "_on_planet_destroyed")
	connect("hurt_planet", planet, "_on_player_hurt_me")
	
	spring.set_node_a(get_path())
	spring.set_node_b(nplanet.get_path())

func _on_planet_destroyed():
	#remove joint
	spring.set_node_a("")
	spring.set_node_b("")
	
	#reset variables
	hookFree = true
	planet = null
	planetHeld = false
	
	
#go back to default idle animation always
func _on_AnimationPlayer_finished():
	mouthAnim.play("idle")

# the mouth sensor for eating
func _on_Area2D_body_enter( body ):
	if not gameOver and body.is_in_group("part"):
		mouthAnim.play("chomp")
		body.call_deferred("queue_free")
		sound.voice_set_pitch_scale(0, rand_range(1,1.5) )
		sound.play("chomp")
		
		#add resources
		get_node("/root/Main").add_resources()


func _on_game_game_over():
	get_node("ocean").hide()
	get_node("ground").hide()
	get_node("mouth").hide()
	get_node("eye").hide()
	get_parent().move_child(self, get_parent().get_child_count()-1)
	gameOver = true
	
	var die = playerDieObj.instance()
	add_child(die)
