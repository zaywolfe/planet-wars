extends TileMap

export(NodePath) var playerPath

var player

func _ready():
	set_process(true)
	randomize()
	
	player = get_node(playerPath)
	print(get_cellv( Vector2(0,0)))
	
	#draw full screen at beginning
	var spArea = OS.get_window_size()
	var pos = player.get_pos()
	var start = world_to_map(pos - spArea/2)
	var end = world_to_map(pos + spArea/2)
	for y in range(start.y, end.y):
		for x in range(start.x, end.x):
			if get_cellv( Vector2(x,y) ) == -1:
				var flipx = false
				var flipy = false
				if( randi()%100 < 49 ): flipx = true
				if( randi()%100 < 49 ): flipy = true
				set_cell( x, y, randi()%100, flipx, flipy)

var x = 99999
var y = 99999
func _process(delta):
	var spArea = OS.get_window_size()
	var pos = player.get_pos()
	var start = world_to_map(pos - spArea/2)
	var end = world_to_map(pos + spArea/2)
	
	#only draw one row per update
	if y > end.y: y = start.y
	
	for x in range(start.x, end.x):
		if get_cellv( Vector2(x,y) ) == -1:
			var flipx = false
			var flipy = false
			if( randi()%100 < 49 ): flipx = true
			if( randi()%100 < 49 ): flipy = true
			set_cell( x, y, randi()%100, flipx, flipy)
	
	y += 1
	
	#remove unneeded
	start -= Vector2(15,15)
	end += Vector2(15,15)
	for cell in get_used_cells():
		if cell.x < start.x or cell.x > end.x:
			set_cellv(cell, -1)
		if cell.y < start.y or cell.y > end.y:
			set_cellv(cell, -1)
