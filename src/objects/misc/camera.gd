extends Camera2D

export(NodePath) var playerPath = null

var player = null
var zMod = 1

func _ready():
	#set flags
	set_process(true)
	
	#set variables
	player = get_node(playerPath)
	

func _process(delta):
	#follow player
	set_pos( Vector2( lerp( get_pos().x, player.get_pos().x, 0.5),
				lerp( get_pos().y, player.get_pos().y, 0.5)))
	
	#zoom in and out based on player speed
	if player.get_linear_velocity().length() > 20:
		if get_zoom() < Vector2(1.8,1.8) * zMod:
			set_zoom( Vector2( lerp( get_zoom().x, 1.8* zMod, 0.5*delta), lerp( get_zoom().y, 1.8* zMod, 0.5*delta) ) )
	else:
		if get_zoom() > Vector2(1,1):
			set_zoom( Vector2( lerp( get_zoom().x, 1, 0.5*delta), lerp( get_zoom().y, 1, 0.5*delta) ) )
