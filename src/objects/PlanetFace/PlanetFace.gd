extends Node2D

export var colors = [Color(0,0,0,0)]

var skyRotateSpeed
var groundRotateSpeed

#components
var base
var land
var sky

func _ready():
	set_process(true)
	
	randomize()
	
	#set components
	base = get_node("base")
	land = get_node("land")
	sky = get_node("sky")
	
	#set random colors
	var color1 = colors[randi()%colors.size()]
	var color2 = colors[randi()%colors.size()]
	var color3 = colors[randi()%colors.size()]
	base.set_modulate(color1)
	#see if there is a land or if it's smooth
	if randi()%101 > 30:
		land.set_modulate(color2)
	else:
		color2 = Color(1,1,1,1)
		land.set_modulate(Color(0,0,0,0))
	
	#do random sky placement
	if randi()%101 > 50:
		#make sure sky is transparent
		color3.a = 0.4
		sky.set_modulate(color3)
	else:
		sky.set_modulate(Color(0,0,0,0))
	
	
	#report colors
	get_parent().report_colors(color1, color2)
	
	#set random speeds
	skyRotateSpeed = rand_range(-1,1)
	groundRotateSpeed = rand_range(-2,2)
	

func _process(delta):
	#rotate planet pars
	land.rotate(groundRotateSpeed*delta)
	sky.rotate(skyRotateSpeed*delta)