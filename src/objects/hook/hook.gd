extends Node2D

#signals
signal hook_lost
signal caught_planet(body)

#objects to instance
var sparksObj = load("objects/sparks/sparks.tscn")

#components
var sound

var attackVector = Vector2(1,0)
var planetIsCaught = false
var killReady = false

func _ready():
	set_process(true)
	get_parent().move_child(self, 0)
	sound = get_node("SamplePlayer")
	sound.play("out")

func _process(delta):
	if not planetIsCaught:
		#add_force(Vector2(0,0), attackVector * 100)
		set_pos( get_pos() + (attackVector * 300) * delta)

#internal functions

func set_fire(vector, rot):
	get_node("Sprite").set_rot(rot)
	attackVector = vector 

func remove_sensor():
	get_node("Sprite").call_deferred("queue_free")

func make_sparks ():
	var sparks = sparksObj.instance()
	sparks.set_pos(get_pos())
	sparks.set_emitting(true)
	get_parent().add_child(sparks)

func get_attach_pos():
	return get_node("Sprite/attachpoint").get_global_pos()

#signals

#alert player when hook is too far away to catch a planet
func _on_DeathTimer_timeout():
	#remove the planet sensor
	if not planetIsCaught:
		emit_signal("hook_lost")
	call_deferred("queue_free")

func _on_planetSensor_body_enter( body ):
	if not body.is_in_group("player"):
		if body.is_in_group("planet"):
			planetIsCaught = true
			emit_signal("caught_planet", body)
			sound.play("attach", true)
			#make sparks
			make_sparks()
			
			#kill hook
			remove_sensor()
			get_node("DeathTimer").stop()
			get_node("DeathTimer").start()
			killReady = true
