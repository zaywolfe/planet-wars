
extends StreamPlayer

export var playlist = StringArray()
var currentSong = ""
var pointer = 0
var islooping = true

func _ready():
	randomize()
	var pointer = randi()%2
	currentSong = load(playlist[pointer])
	set_stream(currentSong)
	play()
	get_node("Timer").start()

func move_next_song ():
	pointer += 1
	if pointer > 1:
		pointer = 0
	currentSong = load(playlist[pointer])
	set_stream(currentSong)
	play()
	get_node("Timer").set_wait_time(get_length() * 3)
	get_node("Timer").start()
	

func _on_GlobalMusic_finished():
	if not islooping:
		move_next_song()


func _on_Timer_timeout():
	move_next_song()
