extends RigidBody2D

export var colors = [Color(0,0,0,0)]
var clock = 0.0

var planetColor
var colorReported = false

func _ready():
	set_process(true)
	randomize()
	
	add_to_group("part")
	set_linear_damp(0)
	set_angular_damp(0)
	set_gravity_scale(0)
	
	#add_force( Vector2(0,0), Vector2( rand_range(-5,5), rand_range(-5,5) ) )
	set_linear_velocity( Vector2( rand_range(-20,20), rand_range(-20,20) ) )
	set_angular_velocity( rand_range(-20,20) )
	
	if colorReported:
		get_node("Sprite").set_modulate(planetColor)
	else:
		get_node("Sprite").set_modulate(colors[randi()%colors.size()])


func _process(delta):
	#kill self if too far away from player
	clock += delta
	if clock > 30:
		if (get_node("/root/Main").player.get_pos() - get_pos()).length() > 5000:
			call_deferred("queue_free")

func set_color(either, orr):
	colorReported = true
	if randi()%101 < 50:
		planetColor = either
	else:
		planetColor = orr