extends CanvasLayer

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

func _ready():
	get_node("Control/Button").grab_focus()


func _on_Button_pressed():
	get_tree().change_scene("scenes/game.tscn")


func _on_Button1_pressed():
	get_tree().quit()


func _on_Button2_pressed():
	get_tree().change_scene("menu/credits.tscn")


func _on_return_pressed():
	get_tree().change_scene("menu/mainmenu.tscn")
