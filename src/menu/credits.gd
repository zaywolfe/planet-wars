extends CanvasLayer

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

func _ready():
	get_node("Control/return").grab_focus()


func _on_return_pressed():
	get_tree().change_scene("menu/mainmenu.tscn")
