extends CanvasLayer

var maxSize = 440
var main
#components
var energyBar
var pluStat
var carStat
var hydStat

func _ready():
	set_process(true)
	#components
	energyBar = get_node("Panel/energy")
	pluStat = get_node("Resources/Plu")
	carStat = get_node("Resources/Car")
	hydStat = get_node("Resources/Hyd")
	main = get_node("/root/Main")

func _process(delta):
	energyBar.set_size( Vector2(440*(main.get_energy()/100.0), 5) )
	pluStat.set_text( String( main.get_plutonium() ) )
	carStat.set_text( String( main.get_carbon() ) )
	hydStat.set_text( String( main.get_hydrogen() ) )
