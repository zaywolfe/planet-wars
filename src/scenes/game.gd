extends Node

signal game_over

var main

var PlanetObj = load("res://objects/planets/planet.tscn")
var clock = 0.0

func _ready():
	set_process(true)
	#reset energy
	main = get_node("/root/Main")
	main.reset_stats()
	#emit_signal("game_over")

func _process(delta):
	clock += delta
	
	if clock > 1:
		clock = 0.0
		main.use_resources()
	
	planet_spawn()
	planet_clean()
	
	if main.energy <= 0:
		emit_signal("game_over")
		#get_tree().change_scene("menu/gameover.tscn")

func planet_spawn ():
	var playerPos = main.player.get_pos()
	var spawnArea = Rect2( playerPos + Vector2(-2500,-2500), Vector2(5000,5000) )
	var playerView = Rect2( playerPos - OS.get_window_size()/2, OS.get_window_size())
	var spawnPos = playerPos + Vector2( rand_range(-2500, 2500), rand_range(-2500, 2500) )
	
	for i in range(0,20):
		if main.planets < 20:
			randomize()
			if spawnArea.has_point(spawnPos):
				if not playerView.has_point(spawnPos):
					var planet = PlanetObj.instance()
					planet.set_pos(spawnPos)
					add_child(planet)

func planet_clean():
	var planetList = get_tree().get_nodes_in_group("planet")
	for planet in planetList:
		if (main.player.get_pos() - planet.get_pos()).length() > 2500:
			planet.call_deferred("queue_free")
			main.planets -= 1
	
	
	