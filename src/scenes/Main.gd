extends Node

var energy = 100.0
var plutonium = 10
var carbon = 15
var hydrogen = 20

var planets = 0
var player = null

func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	pass



#energy stuff
func get_energy():
	return energy

func add_energy(amount):
	energy += amount
	
	if energy > 100:
		energy = 100

func subtract_energy(amount):
	energy -= amount
	
	if energy < 0:
		energy = 0

#plutonium stuff
func get_plutonium():
	return plutonium

func add_plutonium(amount):
	plutonium += amount
	
	if plutonium > 999:
		plutonium = 999

func subtract_plutonium(amount):
	plutonium -= amount
	
	if plutonium < 0:
		plutonium = 0

#carbon stuff
func get_carbon():
	return carbon

func add_carbon(amount):
	carbon += amount
	
	if carbon > 999:
		carbon = 999

func subtract_carbon(amount):
	carbon -= amount
	
	if carbon < 0:
		carbon = 0

#hydrogen stuff
func get_hydrogen():
	return hydrogen

func add_hydrogen(amount):
	hydrogen += amount
	
	if hydrogen > 999:
		hydrogen = 999

func subtract_hydrogen(amount):
	hydrogen -= amount
	
	if hydrogen < 0:
		hydrogen = 0

#global funcs
func reset_stats():
	energy = 100
	plutonium = 10
	carbon = 15
	hydrogen = 20

func add_resources():
	add_plutonium(randi()%6)
	add_carbon(randi()%6)
	add_hydrogen(randi()%6)

func use_resources():
	if plutonium > 0 or carbon > 0 or hydrogen > 0:
		subtract_plutonium(1)
		subtract_carbon(1)
		subtract_hydrogen(1)
		add_energy(5)
	else:
		subtract_energy(2)